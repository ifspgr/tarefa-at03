<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
		<link href="<?= BASEURL ?>assets/mdb/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?= BASEURL ?>assets/mdb/css/mdb.min.css" rel="stylesheet">
		<link href="<?= BASEURL ?>assets/mdb/css/style.css" rel="stylesheet">
		<link href="<?= BASEURL ?>assets/mdb/css/estilo.css" rel="stylesheet">
		<link href="<?= BASEURL ?>assets/img/favicon.png" rel="shortcut icon" type="image/x-icon">
		<title>ツ Site de Upload</title>
	</head>
<body>