<nav class="mb-1 navbar fixed-top navbar-expand-lg navbar-dark unique-color-dark scrolling-navbar">
  <a class="navbar-brand" href="http://localhost/loo/">Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="http://localhost/loo/upload/">Upload
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/loo/upload/lista.php">Visualizar</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/loo/procurar/">Procurar</a>
      </li>

    </ul>

  <form class="form-inline my-1" name="pesquisa" method="post" action="<?= BASEURL ?>procurar/models/pesquisar.php">
    <div class="md-form form-sm my-0">
      <input class="form-control form-control-sm mr-sm-2 mb-0" name="pesquisa" id="pesquisa" type="text" placeholder="Procurar"
        aria-label="Procurar">
    </div>
    <button class="btn btn-outline-white btn-sm my-0" type="submit">Procurar</button>
  </form>
</nav>