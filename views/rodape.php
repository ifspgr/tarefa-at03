
  <footer class="page-footer font-small unique-color-dark pt-4">

    <div class="container">

      <ul class="list-unstyled list-inline text-center py-2">
        <li class="list-inline-item">
          <h5 class="mb-1">Instituto Federal Guarulhos </h5>
        </li>
        <li class="list-inline-item">
          <a href="http://portal.ifspguarulhos.edu.br/" class="btn btn-outline-white btn-rounded"><i class="fas fa-graduation-cap"></i></a>
        </li>
      </ul>
    </div>

    <div class="footer-copyright text-center py-3">© 2019 Copyright:
      <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
    </div>

  </footer>
