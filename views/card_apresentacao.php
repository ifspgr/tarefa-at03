    <br><br>
  <section class="my-5">
    <h2 class=" animated bounce h1-responsive font-weight-bold text-center my-5">Apresentação</h2>
    <p class=" animated slideInRight lead grey-text text-center w-responsive mx-auto mb-5">Este é um site para matéria de Loo, possui 4 paginas, sendo elas:
    Uma de apresentação e com alguns dados pessoais, Uma para upload de arquivos, Uma para vizualização dos arquivos e Outra para busca por palavras-chave. Resumidamente 
    essas paginas tornam mais facil lidar com arquivos.</p>

    <div class="row">
      <div class="col-lg-5 text-center text-lg-left">
        <img class="img-fluid animated zoomIn" src="https://mdbootstrap.com/img/Photos/Others/screens-section.jpg" alt="Sample image">
      </div>

      <div class="col-lg-7">

        <div class="row mb-3">

          <div class="col-1">
            <i class="animated tada fas fa-share fa-lg indigo-text"></i>
          </div>

          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="animated bounceInLeft font-weight-bold mb-3">Upload (crud)</h5>
            <p class="animated bounceInLeft grey-text">A pagina de Upload é a pagina onde arquivos podem ser enviados
            a mim através de um banco de dados.</p>
          </div>
        </div>

        <div class="row mb-3">

          <div class="col-1">
            <i class="animated tada fas fa-share fa-lg indigo-text"></i>
          </div>

          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="animated bounceInRight font-weight-bold mb-3">Vizualizador</h5>
            <p class="animated bounceInRight grey-text">Nesta pagina é possível vizualizar uma tabela com os arquivos
            enviados e seus respectivos nomes.</p>
          </div>
        </div>

        <div class="row">

          <div class="col-1">
            <i class="animated tada fas fa-share fa-lg indigo-text"></i>
          </div>

          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="animated bounceInLeft font-weight-bold mb-3">Buscas</h5>
            <p class="animated bounceInLeft grey-text mb-0">A pagina de buscas torna mais facil encontrar itens através 
            de palavras-chave.</p>
          </div>

        </div>

      </div>

    </div>

  </section>

  <section class="text-center my-5">

    <h2 class="h1-responsive font-weight-bold my-5">Oque deseja?</h2>

    <p class="lead grey-text w-responsive mx-auto mb-5">Navegue na pagina de sua preferência clicando em um dos icones abaixo, ou desca mais um pouco
    para ver algumas informações sobre mim.</p>

    <div class="row">

      <div class="col-md-4">
  
      <a href="<?= BASEURL ?>upload/index.php"><i class="animated rubberBand infinite fas fas fa-upload fa-3x red-text"></i></a>
        <h5 class="font-weight-bold my-4">Upload</h5>
        <p class="grey-text mb-md-0 mb-5">Faça upload de arquivos.
        </p>

      </div>
      <div class="col-md-4">

      <a href="<?= BASEURL ?>vizualizar/index.php"><i class="animated rubberBand infinite fas fa-clipboard-list fa-3x cyan-text"></i></a>
        <h5 class="font-weight-bold my-4">Vizualize</h5>
        <p class="grey-text mb-md-0 mb-5">Vizualize os arquivos.
        </p>

      </div>

      <div class="col-md-4">

        <a href="<?= BASEURL ?>procurar/index.php"> <i class="animated rubberBand infinite fas fa-search fa-3x orange-text"></i></a>
        <h5 class="font-weight-bold my-4">Procure</h5>
        <p class="grey-text mb-0">Procure oque precisa.
        </p>

      </div>
    </div>

  </section>

  <div class="card testimonial-card ">

    <div class="card-up indigo lighten-1"></div>

    <div class="mx-auto mt-4 white">
      <img src="<?= BASEURL ?>assets/img/pessoal1.jpg" class="animated zoomIn avatar rounded-circle" alt="Desenvolvedor">
    </div>

    <div class="card-body">

      <h3 class="card-title text-danger">Pablo da Silva</h3>
      <h6 class="card-title">Criador da Pagina  <i class="far fa-edit"></i></h6>
      <hr>

      <p class="card-text"><a class="text-primary">Nome completo: </a>Pablo da Silva Expedito Xavier.</p>
          <p class="card-text"><a class="text-primary">Idade: </a>18 Anos.</p>
          <p class="card-text"><a class="text-primary">Moradia: </a>Guarulhos, SP.</p>
          <p class="card-text"><a class="text-primary">Sexo: </a>Masculino.</p>
          <p class="card-text"><a class="text-primary">Status de Estudante: </a>Estudante do Instituto Federal Guarulhos.</p>
          <p class="card-text"><a class="text-primary">Prontuário: </a>GU1800094.</p>

    </div>

  </div>
