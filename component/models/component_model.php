<?php

include APPPATH.'component/libraries/component/Card.php';
include APPPATH.'component/libraries/component/Table.php';

// MODEL:
// Geração de dados e operações lógicas.

function get_cards(){
    // $link = new mysqli('localhost','root','','loo');
    // $sql = "SELECT * FROM card WHERE id < 16";
    // $rs = $link->query($sql);
    $db = new DB();
    $v = $db->get('card');
    $html = '';

    foreach($v AS $data){
        $title = $data['titulo']; 
        $content = $data['conteudo'];
        $image = $data['imagem'];
        $label = $data['rotulo_botao'];

        $card = new Card($title, $content, $image, $label); // Corresponde ao que vai acontecer no construtor
        $html .= $card->getHTML();   
    }

    return $html;
}

function get_table(){
    $cols = array('Nome', 'Sobrenome', 'E-Mail', 'Senha', 'Telefone');
    $rows = array(
        array('nome'=>'ghdfgh', 'sobrenome'=>'dghdfhhg', 'email'=>'dghdfhggf', 'senha'=>'35345', 'telefone'=>'34534535'),
        array('nome'=>'dfhhgjgf', 'sobrenome'=>'serter', 'email'=>'sertsetr', 'senha'=>'srtrtset', 'telefone'=>'3453453453'),
        array('nome'=>'szertesrt', 'sobrenome'=>'sertset', 'email'=>'stresert', 'senha'=>'sertser', 'telefone'=>'3453556236')
    );

    $table = new Table($cols, $rows);
    $table->addHeaderClass('orange');
    $table->addTableClass('table-striped table-hover');
    $table->addTableClass('table-sm');
    return $table->getHTML();
}

?>