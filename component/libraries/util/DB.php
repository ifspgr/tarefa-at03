<?php

class DB {
    // Atributos da classe
    // Endereço do site que hospeda o BD
    private $host;

    // Nome de Usuário
    private $user;

    // Senha de acesso ao BD
    private $pass;

    // Nome do Banco de Dados 
    private $name;

    private $link;
    // CONSTRUTOR - INICIALIZA (DA VALOR INICIAL) OS ATRIBUTOS DA CLASSE
    function __construct(){
        $this->host = "localhost";
        $this->user = "root";
        $this->pass = "";
        $this->name = "loo";

        $this->$link = new mysqli($this->host, $this->user, $this->pass, $this->name);
    }

    public function get($table){
        $sql = "SELECT * FROM $table";
        $rs = $this->$link->query($sql);
        $data = array();
        while($row = $rs->fetch_assoc()){
            $data[]= $row;
        }
        return $data;
    }
}

?>