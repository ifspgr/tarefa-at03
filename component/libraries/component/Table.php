<?php

class Table{
    // Atributos
    private $colunas;
    private $conteudo;
    private $headerClasses = '';
    private $tableClasses = '';

    // Construtor
    function __construct($colunas, $conteudo){
        $this->conteudo = $conteudo;       
        $this->colunas = $colunas;
    }

    private function header(){
        $html = '<thead class="'.$this->headerClasses.'"><tr>';        
        // Conteúdo do header
        foreach ($this->colunas AS $coluna){
            $html .= '<th scope="col">'.$coluna.'</th>';
        }

        $html .= '</tr></thead>';
        return $html;
    }

    public function addHeaderClass($class){
        $this->headerClasses .= "$class ";
    }

    public function addTableClass($class){
        $this->tableClasses .= "$class ";
    }

    private function body(){// dummy
        $html = "<tbody>";
        foreach ($this->conteudo AS $linha) {
            $html .= '<tr>';
            foreach ($linha AS $coluna) {
                $html .= '<td scope="col">'.$coluna.'</td>';               
            }
            $html .= '</tr>';
        }
        $html .= "</tbody>";
        return $html;
    }

    // Métodos
    public function getHTML(){
        $html = '<table class="table '.$this->tableClasses.'">';
        $html .= $this->header();
        $html .= $this->body();
        $html .= '</table>';
        return $html;
    }

}

?>