<?php

// CONTROLADOR

include '../config/constants.php';
include APPPATH.'views/header.php';
include APPPATH.'views/navbar.php';

include 'libraries/util/DB.php';
include 'models/component_model.php';
$components = get_table();
include 'views/component_view.php';

// include APPPATH.'views/rodape.php';
include APPPATH.'views/footer.php';

?>