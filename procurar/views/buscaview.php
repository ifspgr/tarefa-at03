<style>
body, html {
  height: 100%;
}
.bg {
  background-image: url("<?= BASEURL ?>assets/img/bg1.gif");

  height: 75%;

  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
</style>
<body>
    <div class="bg"><br><br><br><br>
	<form name="pesquisa" method="post" action="models/pesquisar.php">
		<div class="input-group mt-5 mx auto md-form form-sm form-2 pl-0">
		<input class="form-control my-0 py-1 amber-border animated bounceInDown" name="pesquisa" id="pesquisa" type="text" placeholder="Busque aqui.." aria-label="Search">
			<div class="input-group-append animated bounceInDown">
			   <button class="input-group-text peach-gradient lighten-3" type="submit" id="basic-text1"><i class="fas fa-search text-grey" aria-hidden="true"></i></button>  
			</div>
		</div>
    </form>
<div class="text-white ml-5 animated bounceInRight">
<br><br><h1>Faça aqui sua pesquisa !</h1>
</div>
</div>
</body>