<?php

include_once APPPATH.'libraries/util/DB.php';

class UploadManager{
    private $db;
    // A variável deve ser inicializada

    // Diretório onde os arquivos serão gravados
    private $target_dir = APPPATH."arquivos/";

    function __construct(){
        $this->db = new DB();
    }

    public function done($data){
        $file_name = date('Ymdhis') . '_' . basename($_FILES["arquivo"]["name"]);
        // Definir o nome/caminho (caminho qualificado de acesso ao doc) do arquivo
        $target_file = $this->target_dir . $file_name;

        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $target_file)) {
            $data['arquivo'] = $file_name;
            $this->db->insert('upload', $data);
            return true;
        } else return false;
    }

    public function lista_arquivos(){
        $sql = "SELECT nome, descricao FROM upload";
        return $this->db->query($sql);
    }
    public function remove($id){
        // obtem nome do arquivo
        $sql = "SELECT arquivo FROM upload WHERE id = $id";
        $res = $this->db->query($sql);
        $arq = $res[0]['arquivo'];

        // cria o caminho ate o local onde o arquivo esta armazenado
        $file = $this->target_dir.$arq;

        if(file_exists($file)){
        unset($file);

            // apaga o registro do arquivo no banco de dados
            $sql = "DELETE FROM upload WHERE id = $id";
            $this->db->query($sql);
            return true; 
            }
         
        
        return false;
    }
}

?>