<?php ob_start(); ?>
<br>
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-9">
                <form action="index.php" method="POST" enctype="multipart/form-data">
                    <h3 class="pt-2 mb-4">Selecione um arquivo para enviar</h3>

                    <div class="md-form">
                        <input type="text" class="w-75" name="nome" id="nome">
                        <label for="nome">Nome</label>
                    </div>

                    <div class="md-form">
                        <input type="text" class="w-75" name="descricao" id="descricao">
                        <label for="descricao">Descrição</label>
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Upload de arquivos</span>
                        </div>
                        <div class="custom-file">
                                <input type="file" name="arquivo" id="arquivo" />

                            <label class="custom-file-label" for="arquivo">Escolher arquivo</label>

                        </div>
                    </div>
                    <input class="btn btn-primary mt-4" type="submit" value="Enviar" name="submit" />
                </form>
            </div>
        </div>
    </div>
<?php $upload_form = ob_get_clean(); ?>