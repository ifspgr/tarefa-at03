<?php $upload = ob_start()?>
<div class="container mt-5">
<form class="md-form" id="formulario-upload" method="POST" enctype="multipart/form-data">
  <div class="file-field big">
    <a class="btn-floating btn-lg pink lighten-1 mt-0 float-left">
      <i class="fas fa-paperclip" aria-hidden="true"></i>
      <input type="file" id="arquivos" name="arquivo" multiple>
    </a>
    <div class="file-path-wrapper">
      <input class="file-path validate" type="text" placeholder="Upload one or more files">
    </div>
  </div>
</form>
<form class="md-form" id="formulario-upload" method="POST" enctype="multipart/form-data">
  <div class="file-field big">
    <a class="btn-floating btn-lg cyan darken-2 mt-0 float-left">
      <i class="far fa-heart" aria-hidden="true"></i>
      <input type="file" id="arquivos" name="arquivo"multiple>
    </a>
    <div class="file-path-wrapper">
      <input class="file-path validate" type="text" placeholder="Upload one or more files">
    </div>
  </div>
</form>
<form class="md-form" id="formulario-upload" method="POST" enctype="multipart/form-data">
  <div class="file-field big">
    <a class="btn-floating btn-lg amber darken-2 mt-0 float-left">
      <i class="fas fa-cloud-upload-alt" aria-hidden="true"></i>
      <input type="file" id="arquivos" name="arquivo" multiple>
    </a>
    <div class="file-path-wrapper">
      <input class="file-path validate" type="text" placeholder="Upload one or more files">
    </div>
  </div>
  <input type="submit">
</form>
</div>
<?php $upload = ob_get_clean()?>